//
// Created by bshort092 on 4/13/2017.
//

#ifndef HW7_ITAK_RESULTSET_H
#define HW7_ITAK_RESULTSET_H

#include <iostream>
#include <string>
#include <vector>
#include "Dictionary.h"

using namespace std;

class ResultSet : public Dictionary <string, vector<string>> {

public:
    void print (ostream& os);

    string getKey(){return m_key;}
    vector<string> getValue(){return m_values;}

    void setKey(string key){m_key = key;}
    void setValues (vector<string> values){m_values = values;}

private:
    string m_key;
    vector<string> m_values;
};


#endif //HW7_ITAK_RESULTSET_H
