//
// Created by bshort092 on 4/1/2017.
//

#ifndef HW6_DICTIONARY_KEYVALUE_H
#define HW6_DICTIONARY_KEYVALUE_H

#include <iostream>
using namespace std;

template <typename K, typename V>
class KeyValue {

public:
    //KeyValue<K,V> (K key, V value){m_key = key; m_value = value;}
    KeyValue(){};
    KeyValue(const KeyValue<K,V>& obj);
    K getKey(){return m_key;};
    V getValue(){return m_value;}

    void setKey(K key){ m_key = key;}
    void setValue(V value){m_value = value;}

private:
    K m_key;
    V m_value;
};
template <typename K, typename V>
KeyValue<K,V>::KeyValue(const KeyValue<K,V>& obj)
 {
     m_key = obj.m_key;
     m_value = obj.m_value;
 }
#endif //HW6_DICTIONARY_KEYVALUE_H
