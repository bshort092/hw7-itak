//
// Created by bshort092 on 4/13/2017.
//

#ifndef HW7_ITAK_DENIALOFSERVICEANALYZER_H
#define HW7_ITAK_DENIALOFSERVICEANALYZER_H

#include "Analyzer.h"
#include "Configuration.h"

class DenialOfServiceAnalyzer : public Analyzer {

public:
    virtual ResultSet run(istream& in);


};


#endif //HW7_ITAK_DENIALOFSERVICEANALYZER_H
