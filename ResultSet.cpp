//
// Created by bshort092 on 4/13/2017.
//

#include "ResultSet.h"


void ResultSet::print(ostream &out) {

    vector<string> vector;

    out << "Key:   " << getKey() << endl;

    vector = getValue();

    out << "Value: " << endl;
    for (int i = 0; i < vector.size(); i++)
    {
        out << vector[i] << endl;
    }

}