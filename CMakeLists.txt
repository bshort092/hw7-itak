cmake_minimum_required(VERSION 3.6)
project(hw7_itak)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
        Analyzer.cpp Analyzer.h
        DenialOfServiceAnalyzer.cpp DenialOfServiceAnalyzer.h
        PortScanAnalyzer.cpp PortScanAnalyzer.h
        Configuration.cpp Configuration.h
        ResultSet.cpp ResultSet.h
        Dictionary.h KeyValue.h)

add_executable(hw7_itak main.cpp ${SOURCE_FILES})

set(TEST_FILES
        Testing/testAnalyzer.cpp Testing/testAnalyzer.h
        Testing/testDenialOfServiceAnalyzer.cpp Testing/testDenialOfServiceAnalyzer.h
        Testing/testPortScanAnalyzer.cpp Testing/testPortScanAnalyzer.h
        Testing/testConfiguration.cpp Testing/testConfiguration.h
        Testing/testResultSet.cpp Testing/testResultSet.h)

add_executable(Test Testing/testMain.cpp ${SOURCE_FILES} ${TEST_FILES})
