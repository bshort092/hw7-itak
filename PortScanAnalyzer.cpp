//
// Created by bshort092 on 4/13/2017.
//

#include "PortScanAnalyzer.h"
#include <string>
#include "ResultSet.h"
#include <map>
#include <set>

ResultSet PortScanAnalyzer:: run(istream& in)
{
    //Reading in the file
    string ip;
    int srcPort;
    map <string, set<int>> data;
    map <string, set<int>>:: iterator it;

    while(in.good())
    {
        in.ignore(50, ',');
        getline(in, ip, ',');
        in.ignore(50, ',');
        string src;
        getline(in, src, ',');

        srcPort = stoi(src);

        it = data.find(ip);
        if(it == data.end())
        {
            data.insert(make_pair(ip, set<int>()));
            data[ip].insert(srcPort);
        }
        else
        {
            it->second.insert(srcPort);
        }
    }


}