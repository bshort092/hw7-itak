//
// Created by bshort092 on 4/13/2017.
//

#ifndef HW7_ITAK_PORTSCANANALYZER_H
#define HW7_ITAK_PORTSCANANALYZER_H

#include "Analyzer.h"
#include "Configuration.h"

class PortScanAnalyzer : public Analyzer {

public:
    virtual ResultSet run(istream& in);

};


#endif //HW7_ITAK_PORTSCANANALYZER_H
