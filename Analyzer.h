//
// Created by bshort092 on 4/13/2017.
//

#ifndef HW7_ITAK_ANALYZER_H
#define HW7_ITAK_ANALYZER_H

#include <iostream>
#include "ResultSet.h"
#include "Configuration.h"

class Analyzer {
public:
    virtual ResultSet run(istream& in) = 0;

    void setConfig(Configuration obj){m_configuration = obj;}

private:
    Configuration m_configuration;
};


#endif //HW7_ITAK_ANALYZER_H
