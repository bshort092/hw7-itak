//
// Created by bshort092 on 4/13/2017.
//

#ifndef HW7_ITAK_CONFIGURATION_H
#define HW7_ITAK_CONFIGURATION_H

#include "Dictionary.h"

class Configuration : public Dictionary<string, string> {

    string getAsString();
    int getAsInt();
    double getAsDouble();
};


#endif //HW7_ITAK_CONFIGURATION_H
